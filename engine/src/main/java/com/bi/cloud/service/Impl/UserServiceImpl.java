package com.bi.cloud.service.Impl;

import com.bi.cloud.dao.UserDao;
import com.bi.cloud.pojo.Users;
import com.bi.cloud.service.UserService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Service
@Component
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public Users userInfo(String username) {
        return userDao.getUsers(username);
    }
}
